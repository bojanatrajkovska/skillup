function showcardcontent(moviecards_id, carticka_id) {
    let content = document.getElementById(moviecards_id);
    let card = document.getElementById(carticka_id);

    content.style.display = "block";
    card.style.borderColor = "green";

                                      
}
function hidecardcontent(moviecards_id, carticka_id,) {
    let content = document.getElementById(moviecards_id);
    let card = document.getElementById(carticka_id);

    content.style.display = "none";
    card.style.borderColor = "white";
}





function viewDetails(url){
    window.location = url;
};


function createCards(divId, movies){
    for(let i = 0; i < movies.length; i++){
        createCard(divId, movies[i], i)
    }
}

function createCard(cardC, movie, index){
    let maindiv = document.createElement("div");
    maindiv.classList.add("col-3");
    maindiv.classList.add("card-holder");
  
    let cards = document.createElement("div");
    cards.classList.add("movie-card");
    cards.id = "moviecards_" + index;




    cards.style.backgroundImage = "url(" + movie.medium_cover_image + ")";

  cards.addEventListener("mouseover",
  function(){
    showcardcontent("carticka_" + index, "moviecards_" + index)
  }
  );

  cards.addEventListener("mouseleave",
  function(){
    hidecardcontent("carticka_" + index, "moviecards_" + index)
  }
  );
    


    let card_Content = document.createElement("div");
    card_Content.classList.add("card-content");
    card_Content.id = "carticka_" + index;
  
     let zvezda = document.createElement("i");
     zvezda.classList.add("fas");
     zvezda.classList.add("fa-star");
     zvezda.classList.add("star-card");
    
    let paragrav = document.createElement("p");
    paragrav.classList.add("rating");
    paragrav.innerText =movie.rating + '7/10';
  
    let paragrav2 = document.createElement("p");
    paragrav2.classList.add("genre");
    paragrav2.innerText = movie.genres +'Action';
    
    let buttonmovie = document.createElement('button');
    buttonmovie.classList.add("btn");
    buttonmovie.classList.add("btn-success");
    buttonmovie.classList.add("details-btn");
    buttonmovie.innerText = "View details";
    buttonmovie.addEventListener("click",


    function(){
        viewDetails("moviedetails.html?movie_id=" + movie.id);
    }
    );


  
    card_Content.append(zvezda);
    card_Content.append(paragrav);
    card_Content.append(paragrav2);
    card_Content.append(buttonmovie);
    
    cards.append(card_Content);
    maindiv.append(cards);
  
  
    let cardContainer = document.getElementById(cardC);
    console.log(cardContainer);
    cardContainer.append(maindiv);
    
  
  
   
  }
  