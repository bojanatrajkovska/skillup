$(document).ready(function(){
    $('#browseForm').submit( function() {
        $.ajax({
            url     : 'https://yts.mx/api/v2/list_movies.json',
            type    : 'GET',
            data    : $('#browseForm').serialize(),
            success : function( response ) {
                        console.log('response: ', response );

                        document.getElementById("movieCount").innerText = response.data.movie_count;
                       
                       
                        document.getElementById("cardWrapper").innerHTML = ''; 

                        
                        createCards('cardWrapper', response.data.movies);

                      }
        });

        return false;
    });

    $('#browseForm').submit();

});