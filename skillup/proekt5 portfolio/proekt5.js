function showPortfolioItems(className){
   
    let allItems = document.getElementsByClassName('all');
    if(className == 'all'){
        for(let item of allItems){
            item.style.display = 'block';
        }
    }
    else{
        for(let item of allItems){
            item.style.display = 'none';
        }
    }
    let items = document.getElementsByClassName(className);
    for(let item of items){
        item.style.display = 'block';
    }
}