body {
    margin: 0;
    padding: 0;
    font-family:  Arial,Helvetica,freesans,sans-serif;
}
#logo{
    width: 200px;
}
#logo img{
  width: 90px;  
  padding: 5px 20px 2px 100px;
}
a{
    text-decoration: none;
}
.border{
    border-right: solid 1px #cccccc;
    border-bottom: solid 1px #cccccc;
}
.fl{
    float: left;
}
.header-font-color,
.menuitems,
#searchinput::placeholder
{
    color: rgb(51,51,51);
}
#sing-in{
    display: inline-block;
    padding: 0px 80px 10px 14px;
    font-size: 13px;
    font-weight: bold;
}
#sing-in img{
    width:33px;
    position: relative;
    top: 6px;
}
#sing-in:hover{
    border-bottom: 4px solid #1188bb;
    padding: 4px 80px 7px 14px;
}
#menu-ul {
    margin:0;
}
#menu-ul li {
    list-style: none;
}
.menuitems{
    padding: 12px 20px;
    font-size: 13px;
    font-weight: bold;
    display: inline-block;
}
.menuitems:hover{
    border-bottom-width: 4px; 
    border-bottom-style: solid;
    padding: 12px 20px 8px 20px
}
#menu-item2 a {
    border-bottom-color: #bb1919;
}
#menu-item3 a {
    border-bottom-color: #ffd230;
}
#menu-item4 a {
    border-bottom-color: #0068ff;
}
#menu-item5 a {
    border-bottom-color: #0052a1;
}
#menu-item6 a {
    border-bottom-color: #589e50;
}
#menu-item7 a {
    border-bottom-color: #002856;
}
#menu-item8 a {
    border-bottom-color: #482878;
}
#menu-item9 a {
    border-bottom-color: #262626;
}
#arrow-down{
    width: 6px;
    margin-left: 60px;
    position: relative;
    bottom: 1px;
}
#searchinput::placeholder{
    font-weight: bold;
}
#searchinput{
    padding: 3px 0px 3px 7px;
    margin-left: 17px;
    margin-top: 8px;
    background-color: #E4E4E4;
    border: 0px;
}
#searchimg{
    width: 17px;
    position: relative;
    top: 5px;
    left: -23px;
}
#headernews{
    width: 100%;
    margin-top: 0px;
    background-color: #BB1919;
    font-size: 45px;
    color: #ffffff;
    padding: 5px 2px 3px 191px
 }
 #headernews a{
     color: #ffffff;
 }
#redmenu{
    width: 100%;
    background-color: #BB1919;
    display: inline-block;
    padding-left:140px;
    padding-top: 5px;
}
#redmenu-ul{
    margin: 0px;
}
#redmenu-ul li{
    list-style:none
}
.redmenu-items{
    color: #fff;
    font-size: 14px;
    padding:10px;
}
.redmenu-items:hover{
    border-bottom:solid 3px #fff;
    margin-bottom: 1px;
}